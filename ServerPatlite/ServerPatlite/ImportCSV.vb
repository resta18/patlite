﻿Imports System.IO
Imports System.IO.File
Imports System.String
Imports System.Net
Imports System.Data.OleDb
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections.Specialized
Public Class ImportCSV
    Dim File As String
    Dim TimeProgress As Integer
    Dim StrWaktu As String = ""
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        LblTimer.Text = Format(Now, "yyyy-MM-dd hh:mm:ss")
       


        If Button1.Text <> "START" Then
            TimeProgress = TimeProgress + 1
            If TimeProgress = 1 Then
                LblStatus.Text = "RECORDING"
            ElseIf TimeProgress = 2 Then
                LblStatus.Text = "RECORDING ."
            ElseIf TimeProgress = 3 Then
                LblStatus.Text = "RECORDING .."
            ElseIf TimeProgress = 4 Then
                LblStatus.Text = "RECORDING ..."
            ElseIf TimeProgress = 5 Then
                LblStatus.Text = "RECORDING ... "
            ElseIf TimeProgress = 6 Then
                LblStatus.Text = "RECORDING ... ."
            ElseIf TimeProgress = 7 Then
                LblStatus.Text = "RECORDING ... .."
            ElseIf TimeProgress = 8 Then
                LblStatus.Text = "RECORDING ... ..."
            Else
                TimeProgress = 0
            End If

        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LblPath.Text = "" Then
            ' MsgBox("Lokasi Folder tidak di temukan")
            LBLERRMSG.Text = "Lokasi Folder tidak di temukan"
            Exit Sub

        End If

        If Button1.Text = "STOP" Then

            'Dim afile As IO.File
            'If afile.Exists("path") Then
            '    'Do whatever
            'Else
            '    'Do whatever else
            'End If



            LblStatus.Text = "STOP"
            Button1.Text = "START"
        Else
            Dim dir As New IO.DirectoryInfo(LblPath.Text)
            If dir.Exists Then

            Else
                'MsgBox("Folder tidak di temukan")
                LBLERRMSG.Text = "Folder tidak di temukan"
                Exit Sub
            End If
            LblStatus.Text = "RECORDING"
            Button1.Text = "STOP"


        End If

    End Sub


    Private Sub ImportCSV_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MsgBox("Menutup aplikasi akan menghentikan perekaman ke dalam database", MsgBoxStyle.Exclamation + MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then
            e.Cancel = True
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
    

        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            LblPath.Text = FolderBrowserDialog1.SelectedPath
            'My.Settings.Reload = FolderBrowserDialog1.SelectedPath



        End If
    End Sub


    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        NotifyIcon1.Visible = True
        Me.Hide()
        NotifyIcon1.BalloonTipText = "Import data Patlite"
        NotifyIcon1.ShowBalloonTip(500)

    End Sub

    Private Sub Import_CSV()


        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")
        File = LblPath.Text & "\patlog_auto" & dateAsString & ".csv"




        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    'MsgBox("Line " & ex.Message & _
                    '"is not valid and will be skipped.")
                    LBLERRMSG.Text = "Line " & ex.Message & _
                    "is not valid and will be skipped."
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()
      

        '
        

    End Sub
    Private Sub Import_CSVPerJam7()


        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now() '.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")
        File = LblPath.Text & "\patlog_auto" & dateAsString & ".csv"




        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    'MsgBox("Line " & ex.Message & _
                    '"is not valid and will be skipped.")
                    LBLERRMSG.Text = "Line " & ex.Message & _
                    "is not valid and will be skipped."
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()


        ''


    End Sub
    Private Sub Import_CSVAdditional()


        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")

       
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            Dim fInfo = New System.IO.FileInfo(OpenFileDialog1.FileName)
           
            File = fInfo.DirectoryName & "\" & fInfo.Name

        Else
            Exit Sub
        End If

        lblsaved.Text = StrWaktu
        Me.Cursor = Cursors.WaitCursor


        ' File = LblPath.Text '& "\patlog_auto" & dateAsString & ".csv"


        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    'MsgBox("Line " & ex.Message & _
                    '"is not valid and will be skipped.")
                    LBLERRMSG.Text = "Line " & ex.Message & _
                    "is not valid and will be skipped."
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()


        ''


    End Sub
    Private Sub Simpan1()
        Call koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()

        Dim connection As New Data.SqlClient.SqlConnection
        Dim command As New Data.SqlClient.SqlCommand

        connection.ConnectionString = str '"Server= server; Database= DB; integrated security=true"
        command.CommandText = "INSERT INTO Patlite_import (DataTime, MacAddress, UserName, SignalRed,SignalYellow,SignalGreen,SignalBlue,SignalWhite,FileImport,TanggalImport) VALUES (@DataTime, @MacAddress, @UserName, @SignalRed,@SignalYellow,@SignalGreen,@SignalBlue, @SignalWhite, @FileImport, @TanggalImport)"

        command.Parameters.Add("@DataTime", SqlDbType.DateTime)
        command.Parameters.Add("@MacAddress", SqlDbType.NVarChar)
        command.Parameters.Add("@UserName", SqlDbType.NVarChar)
        command.Parameters.Add("@SignalRed", SqlDbType.Int)
        command.Parameters.Add("@SignalYellow", SqlDbType.Int)
        command.Parameters.Add("@SignalGreen", SqlDbType.Int)
        command.Parameters.Add("@SignalBlue", SqlDbType.Int)
        command.Parameters.Add("@SignalWhite", SqlDbType.Int)
        command.Parameters.Add("@FileImport", SqlDbType.Text)
        command.Parameters.Add("@TanggalImport", SqlDbType.DateTime)


        connection.Open()
        command.Connection = connection

        For i As Integer = 0 To DataGridView1.Rows.Count - 1

            command.Parameters(0).Value = DataGridView1.Rows(i).Cells(0).Value
            command.Parameters(1).Value = DataGridView1.Rows(i).Cells(1).Value
            command.Parameters(2).Value = DataGridView1.Rows(i).Cells(2).Value
            command.Parameters(3).Value = DataGridView1.Rows(i).Cells(3).Value
            command.Parameters(4).Value = DataGridView1.Rows(i).Cells(4).Value
            command.Parameters(5).Value = DataGridView1.Rows(i).Cells(5).Value
            command.Parameters(6).Value = DataGridView1.Rows(i).Cells(6).Value
            command.Parameters(7).Value = DataGridView1.Rows(i).Cells(7).Value
            command.Parameters(8).Value = DataGridView1.Rows(i).Cells(8).Value
            command.Parameters(9).Value = Now() 'DataGridView1.Rows(i).Cells(9).Value

            If i <> DataGridView1.Rows.Count - 1 Then
                command.ExecuteNonQuery()
            End If
        Next
    End Sub
    Private Sub ImportCSV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim reader As New System.Configuration.AppSettingsReader
        LblPath.Text = reader.GetValue("URL", GetType(String))
    End Sub

 

    Private Sub LblTimer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblTimer.TextChanged
        StrWaktu = Format(Now, "hh:mm:ss")
        Dim reader As New System.Configuration.AppSettingsReader

        If RTrim(StrWaktu.Substring(StrWaktu.Length - 8, 8)) = RTrim(reader.GetValue("TIMEUP", GetType(String))) Then

            If Button1.Text = "STOP" Then

                Import_CSV()
                lblsaved.Text = StrWaktu
            End If
        End If
        If RTrim(StrWaktu.Substring(StrWaktu.Length - 8, 8)) = RTrim(reader.GetValue("TIMEDOWN", GetType(String))) Then

            If Button1.Text = "STOP" Then

                Import_CSVPerJam7()
                lblsaved.Text = StrWaktu
            End If
        End If

    End Sub

    Private Sub LblTimer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblTimer.Click

    End Sub

    Private Sub BtnAdditionalImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdditionalImport.Click

        Call Import_CSVAdditional()
        Me.Cursor = Cursors.Default

    End Sub
End Class
