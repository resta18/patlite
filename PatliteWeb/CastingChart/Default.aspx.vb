﻿
Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.EventArgs

Public Class Patlite
    Inherits Global.System.Web.UI.Page
    Protected TestsTRING As String
    Protected MesinDC01, MesinDC02, MesinDC03, MesinDC04, MesinDC05, MesinDC06, MesinDC07, MesinDC08, MesinDC09, MesinDC10 As String
    Protected MesinDC11, MesinDC12, MesinDC13, MesinDC14, MesinDC15, MesinDC16, MesinDC17, MesinDC18, MesinDC19, MesinDC20 As String
    Protected TanggalProduksi As Date
    Dim rootWebConfig As System.Configuration.Configuration

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Call PanggildataHariini()
        End If

    End Sub
    Public Function GetTimeline() As DataTable
        Dim constr As String = ConfigurationManager.ConnectionStrings("KoneksiPatlite").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("SELECT * FROM SignalPatlite_Timeline")
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    Using dt As New DataTable()
                        dt.TableName = "SignalPatlite_Timeline"
                        sda.Fill(dt)
                        Return dt
                    End Using
                End Using
            End Using
        End Using
    End Function

    Private Sub PanggildataHariini()

        Dim Tgl As String
        Dim sqlConnectionString As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionInfo")
        Dim sqlConnection As SqlClient.SqlConnection = New SqlClient.SqlConnection(sqlConnectionString)
        ' Dim sqlCommand As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT Mesin, TanggalAwal, Case When Cast(TanggalAkhir as DATE) <>Cast(TanggalAwal as DATE) Then Cast(CAst(TanggalAwal as DATE) as DATEtime ) + CAST('00:00:00.000' as DateTime) Else TanggalAkhir END as TangglAkhir2, PartNumber, PartName, Cavity, Qty, cast(TanggalAwal as date )  TanggalTimeLine   FROM SignalPatlite_Timeline Where cast(TanggalAwal as date )=@tanggalshift  Order by Mesin , TanggalAwal ASC")
        Dim sqlCommand As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT Mesin, TanggalAwal, TanggalAkhir as TangglAkhir2, PartNumber, PartName, Cavity, Qty, cast(TanggalAwal as date )  TanggalTimeLine, Case When Cast(TanggalAkhir as DATE) <>Cast(TanggalShift as DATE) Then 1 else 0 end as besok, jenis, jenisdesc   FROM SignalPatlite_Timeline Where cast(TanggalShift as date )=@tanggalshift  Order by Mesin , TanggalAwal ASC")


        Tgl = Format(Now, "yyyy-MM-dd")

        If datepicker.Text = "" Then
            datepicker.Text = Now.ToString("yyyy-MM-dd")
        End If

        Dim sqlParam As SqlClient.SqlParameter = New SqlClient.SqlParameter("@tanggalshift", Tgl) 'CInt(txtCustomerId.Text)
        '
        ' "return  days[date.getDay()] + ", " + months[date.getMonth()] + " " + date.getDate() +"th - " + date.getFullYear() ;"

        'TanggalProduksi = Now().ToString("ddd, yyyy-MM-dd")

        sqlCommand.Parameters.Add(sqlParam)
        sqlCommand.Connection = sqlConnection

        sqlConnection.Open()
        Dim dr As SqlClient.SqlDataReader

        dr = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection)
        TanggalProduksi = CDate(datepicker.Text) 'datepicker.Text
        'TestsTRING = "{ code: 'WARM', description: 'Warming Up Data xAX', begin: createDate('08:00'), end: createDate('08:10') },"

        While dr.Read
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-01" Then
                MesinDC01 = MesinDC01 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC01 = MesinDC01 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC01 = MesinDC01 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If

            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-02" Then
                MesinDC02 = MesinDC02 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC02 = MesinDC05 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC02 = MesinDC05 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If

            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-03" Then
                MesinDC03 = MesinDC03 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC03 = MesinDC03 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC03 = MesinDC03 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If

            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-04" Then
                MesinDC04 = MesinDC04 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC04 = MesinDC04 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC04 = MesinDC04 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If


            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-05" Then
                MesinDC05 = MesinDC05 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC05 = MesinDC05 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC05 = MesinDC05 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-06" Then
                MesinDC06 = MesinDC06 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC06 = MesinDC06 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC06 = MesinDC06 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-07" Then
                MesinDC07 = MesinDC07 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC05 = MesinDC07 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC05 = MesinDC07 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-08" Then
                MesinDC08 = MesinDC08 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC08 = MesinDC08 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC08 = MesinDC08 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-09" Then
                MesinDC09 = MesinDC09 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC09 = MesinDC09 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC09 = MesinDC09 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-10" Then
                MesinDC10 = MesinDC10 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC10 = MesinDC10 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC10 = MesinDC10 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-11" Then
                MesinDC11 = MesinDC11 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC11 = MesinDC11 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC11 = MesinDC11 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-12" Then
                MesinDC12 = MesinDC12 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC12 = MesinDC12 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC12 = MesinDC12 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-13" Then
                MesinDC13 = MesinDC13 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC13 = MesinDC13 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC13 = MesinDC13 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-14" Then
                MesinDC14 = MesinDC14 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC14 = MesinDC14 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC14 = MesinDC14 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-15" Then
                MesinDC15 = MesinDC15 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC15 = MesinDC15 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC15 = MesinDC15 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-16" Then
                MesinDC16 = MesinDC16 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC16 = MesinDC12 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC16 = MesinDC12 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-17" Then
                MesinDC17 = MesinDC17 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC17 = MesinDC17 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC17 = MesinDC17 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-18" Then
                MesinDC18 = MesinDC18 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC18 = MesinDC18 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC18 = MesinDC18 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-19" Then
                MesinDC19 = MesinDC19 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC19 = MesinDC19 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC19 = MesinDC19 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-20" Then
                MesinDC20 = MesinDC20 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "',  begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok")), Integer) & ") },"
                'If Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") = "00:00" Then
                '    MesinDC20 = MesinDC20 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('24:00') },"
                'Else
                '    MesinDC20 = MesinDC20 & "{ code: 'RUN', description: 'Running', begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ") },"
                'End If
            End If
            'Label1.Text = CType(dr.GetValue(dr.GetOrdinal("SubjectName")), String)


            End
            While True

            End While


            dr.Close()
    End Sub

    Protected Sub Btn1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn1.Click

        Dim Tgl As String
        Dim sqlConnectionString As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionInfo")
        Dim sqlConnection As SqlClient.SqlConnection = New SqlClient.SqlConnection(sqlConnectionString)
        'Dim sqlCommand As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT Mesin, TanggalAwal, Case When Cast(TanggalAkhir as DATE) <>Cast(TanggalAwal as DATE) Then Cast(CAst(TanggalAwal as DATE) as DATEtime ) + CAST('00:00:00.000' as DateTime) Else TanggalAkhir END as TangglAkhir2, PartNumber, PartName, Cavity, Qty, cast(TanggalAwal as date )  TanggalTimeLine   FROM SignalPatlite_Timeline Where cast(TanggalAwal as date )=@tanggalshift  Order by Mesin , TanggalAwal ASC")
        Dim sqlCommand As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT Mesin, TanggalAwal, TanggalAkhir as TangglAkhir2, PartNumber, PartName, Cavity, Qty, cast(TanggalAwal as date )  TanggalTimeLine, Case When Cast(TanggalShift as DATE) <>Cast(TanggalAwal as DATE) Then 1 else 0 end as besok1,Case When Cast(TanggalShift as DATE) <>Cast(TanggalAkhir as DATE) Then 1 else 0 end as besok2   , jenis, jenisdesc  FROM SignalPatlite_Timeline Where cast(TanggalShift as date )=@tanggalshift  Order by Mesin , TanggalAwal ASC")
        Dim expenddt As Date
        Try

            If datepicker.Text = "" Then
                Date.TryParseExact(Now.ToString, "yyyy-MM-dd",
                    System.Globalization.DateTimeFormatInfo.InvariantInfo,
                    Globalization.DateTimeStyles.None, expenddt)

            Else
                Date.TryParseExact(datepicker.Text, "yyyy-MM-dd",
                System.Globalization.DateTimeFormatInfo.InvariantInfo,
                Globalization.DateTimeStyles.None, expenddt)


            End If

            'Format(Now, "YYYY-mm-dd")
            If datepicker.Text = "" Then
                datepicker.Text = Now.ToString("yyyy-MM-dd")
            End If
            Try
                Tgl = Format(Now, datepicker.Text)
            Catch ex As Exception

                MsgBox(ex.Message)
                datepicker.Text = Now.ToString("yyyy-MM-dd")
                Call PanggildataHariini()
                Exit Sub
            End Try

            Dim sqlParam As SqlClient.SqlParameter = New SqlClient.SqlParameter("@tanggalshift", Tgl) 'CInt(txtCustomerId.Text)






            sqlCommand.Parameters.Add(sqlParam)
            sqlCommand.Connection = sqlConnection

            sqlConnection.Open()
            Dim dr As SqlClient.SqlDataReader

            dr = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection)
            TanggalProduksi = CDate(datepicker.Text) 'datepicker.Text
            'TanggalProduksi = expenddt ' datepicker.Text
            'MsgBox(datepicker.Text)
            'MsgBox(expenddt)
            'TestsTRING = "{ code: 'WARM', description: 'Warming Up Data xAX', begin: createDate('08:00'), end: createDate('08:10') },"

            While dr.Read
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-01" Then

                    MesinDC01 = MesinDC01 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If

                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-02" Then
                    MesinDC02 = MesinDC02 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If

                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-03" Then
                    MesinDC03 = MesinDC03 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If

                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-04" Then
                    MesinDC04 = MesinDC04 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If


                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-05" Then
                    MesinDC05 = MesinDC05 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-06" Then
                    MesinDC06 = MesinDC06 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' ,begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-07" Then
                    MesinDC07 = MesinDC07 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-08" Then
                    MesinDC08 = MesinDC08 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-09" Then
                    MesinDC09 = MesinDC09 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-10" Then
                    MesinDC10 = MesinDC10 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-11" Then
                    MesinDC11 = MesinDC11 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-12" Then
                    MesinDC12 = MesinDC12 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-13" Then
                    MesinDC13 = MesinDC13 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-14" Then
                    MesinDC14 = MesinDC14 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-15" Then
                    MesinDC15 = MesinDC15 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-16" Then
                    MesinDC16 = MesinDC16 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-17" Then
                    MesinDC17 = MesinDC17 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-18" Then
                    MesinDC18 = MesinDC18 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                'If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-19" Then
                '    MesinDC19 = MesinDC19 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"

                'End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-19" Then
                    MesinDC19 = MesinDC19 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If
                If CType(dr.GetValue(dr.GetOrdinal("Mesin")), String) = "DC-20" Then
                    MesinDC20 = MesinDC20 & "{ code: '" & CType(dr.GetValue(dr.GetOrdinal("jenis")), String) & "', description: '" & CType(dr.GetValue(dr.GetOrdinal("jenisdesc")), String) & "' , begin: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TanggalAwal")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok1")), Integer) & "), end: createDate('" & Format(Hour(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & ":" & Format(Minute(CType(dr.GetValue(dr.GetOrdinal("TangglAkhir2")), Date)), "00") & "'," & CType(dr.GetValue(dr.GetOrdinal("besok2")), Integer) & ") },"


                End If



            End While
            Call TambahDataKosong()
            dr.Close()

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub
    Private Sub TambahDataKosong()
        'If MesinDC01 = "" Then
        '    MesinDC01 = "{ code: 'NONE', description: 'Data tidak ada', begin: createDate('07:00'), end: createDate('07:00',1) },"
        'End If
        'If MesinDC02 = "" Then
        '    MesinDC02 = "{ code: 'NONE2', description: 'Data tidak ada', begin: createDate('07:00'), end: createDate('07:00',1) },"
        'End If

        If MesinDC01 = "" Then
            MesinDC01 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC02 = "" Then
            MesinDC02 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC03 = "" Then
            MesinDC03 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC04 = "" Then
            MesinDC04 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC05 = "" Then
            MesinDC05 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC06 = "" Then
            MesinDC06 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC07 = "" Then
            MesinDC07 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC08 = "" Then
            MesinDC08 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC09 = "" Then
            MesinDC09 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC10 = "" Then
            MesinDC10 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC11 = "" Then
            MesinDC11 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC12 = "" Then
            MesinDC12 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC13 = "" Then
            MesinDC13 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC14 = "" Then
            MesinDC14 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC15 = "" Then
            MesinDC15 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC16 = "" Then
            MesinDC16 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC17 = "" Then
            MesinDC17 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC18 = "" Then
            MesinDC18 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC19 = "" Then
            MesinDC19 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
        If MesinDC20 = "" Then
            MesinDC20 = "{ code: 'STOP', description: 'STOP', begin: createDate('07:00'), end: createDate('07:00',1) },"
        End If
    End Sub

End Class