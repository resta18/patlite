﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CastingChart.Patlite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">
    <title>Produksi Casting - IHARA</title>
</head>

<body>


    <form id="form1" runat="server">




      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <style>
      body {
        font-family: Roboto, Times
      }
          
      h1 {
        display: inline-block;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="dist/jquery.stacked-gantt.css">
  	<script src="libs/jquery/jquery.js"></script>
  	<script src="dist/jquery.stacked-gantt.min.js"></script>
  	<script>

    function createDate(time, daysToSum)
    {
      var split = time.split(':');
      var ret = new Date();
      ret.setHours(split[0]);
      ret.setMinutes(split[1]);

      if(daysToSum) ret.setDate(ret.getDate()+daysToSum);
      return ret;
    }

    var data = [
      {
        description: 'DC-01',
        JumlahStopLine: '0',
        markers: [ ],
        activities: [
          <%=  MesinDC01 %>
   
       
        ]
      },
      {
        description: 'DC-02',
        activities:  [
        <%=  MesinDC02 %>
        ]
      },
      {
        description: 'DC-03',
        activities:  [
        <%=  MesinDC03 %>
        ]
      },
	  {
        description: 'DC-04',
        activities: [
         <%=  MesinDC04 %>
        ]
      },
	  {
        description: 'DC-05',
        activities: [
          <%=  MesinDC05 %>
        ]
      },
	  {
        description: 'DC-06',
        activities: [
        <%=  MesinDC06 %>
        ]
      },
	  {
        description: 'DC-07',
        activities: [
        <%=  MesinDC07 %>
        ]
      },
	  {
        description: 'DC-08',
        activities: [
        <%=  MesinDC08 %>
        ]
      },
	  {
        description: 'DC-09',
        activities: [
        <%=  MesinDC09 %>
        ]
      },
	  {
        description: 'DC-10',
        activities: [
        <%=  MesinDC10 %>
        ]
      },
	  {
        description: 'DC-11',
        activities: [
        <%=  MesinDC11 %>
        ]
      },
	  {
        description: 'DC-12',
        activities: [
        <%=  MesinDC12 %>
        ]
      },
	  {
        description: 'DC-13',
        activities:  [
        <%=  MesinDC13 %>
        ]
      },
	  {
        description: 'DC-14',
        activities:  [
        <%=  MesinDC14 %>
        ]
      },
	  {
        description: 'DC-15',
        activities:  [
        <%=  MesinDC15 %>
        ]
      },
	  {
        description: 'DC-16',
        activities:  [
        <%=  MesinDC16 %>
        ]
      },
	  {
        description: 'DC-17',
        activities:  [
        <%=  MesinDC17 %>
        ]
      },
	  {
        description: 'DC-18',
        activities:  [
        <%=  MesinDC18 %>
        ]
      },
	  {
        description: 'DC-19',
        activities: [
        <%=  MesinDC19 %>
        ]
      },
	  {
        description: 'DC-20',
        activities:  [
        <%=  MesinDC20 %>
            
        ]
      },
	  
    ];

    var generalMarkers = [
    ];

    
    var options = {
      data: data,
      generalMarkers: generalMarkers,
      style: {
        months: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
        activityStyle: {
		  'RUN': { color: "#1ad307" },  
		  'STOP': { color: "#f61111" },
		  'MOULD': { color: "#0000cc" },
		  'PLANSTOP': { color: "#FF9900" },
		  'MACHINE': { color: "#f548cb" },
		  'OTHER': { color: "#db48f5" },
		  'WARM': { color: "#fb8b04"},
		  'NONE': { color: "#e0e0e0"},
          'NONE2': { color: "#f0f0f0"},
        },
        showDateOnHeader: true,
        dateHeaderFormat: function(date)
        {
          var days = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
          var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
          var d = new Date('<%=  TanggalProduksi %>');
          
          // return days[date.getDay()] ;0 i
           return days[d.getDay()] ;
        },
        descriptionContainerWidth: '80px'
        
      }
    };

  	$(document).ready(function() {
      var $timeline = $('#timeline').stackedGantt(options);



  	});

  	</script>

    <div>
      <h1>Produksi Casting</h1><hr>
    </div>
    
    <div>
     
       <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
      $(function () {
          $("#datepicker").datepicker();
          
              $("#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
          
      });
  </script>
  <p style="text-align: center;">Tanggal Produksi  : 
      <asp:TextBox ID="datepicker" runat="server"></asp:TextBox>
      <asp:Button 
          ID="Btn1" runat="server" Text="Submit" />
        </p>
  
 

    </br>
  	<div id="timeline" style="width: 100%"></div>

    </form>
</body>
</html>



