﻿Imports System.IO
Imports System.IO.File
Imports System.String
Imports System.Net
Imports System.Data.OleDb
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections.Specialized
Public Class ImportCSV
    Dim File As String
    Dim TimeProgress As Integer
    Dim StrWaktu As String = ""
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        LblTimer.Text = Format(Now, "yyyy-MM-dd hh:mm:ss")
       


        If Button1.Text <> "START" Then
            TimeProgress = TimeProgress + 1
            If TimeProgress = 1 Then
                LblStatus.Text = "RECORDING"
            ElseIf TimeProgress = 2 Then
                LblStatus.Text = "RECORDING ."
            ElseIf TimeProgress = 3 Then
                LblStatus.Text = "RECORDING .."
            ElseIf TimeProgress = 4 Then
                LblStatus.Text = "RECORDING ..."
            ElseIf TimeProgress = 5 Then
                LblStatus.Text = "RECORDING ... "
            ElseIf TimeProgress = 6 Then
                LblStatus.Text = "RECORDING ... ."
            ElseIf TimeProgress = 7 Then
                LblStatus.Text = "RECORDING ... .."
            ElseIf TimeProgress = 8 Then
                LblStatus.Text = "RECORDING ... ..."
            Else
                TimeProgress = 0
            End If

        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LblPath.Text = "" Then
            MsgBox("Lokasi Folder tidak di temukan")
            Exit Sub

        End If

        If Button1.Text = "STOP" Then

            'Dim afile As IO.File
            'If afile.Exists("path") Then
            '    'Do whatever
            'Else
            '    'Do whatever else
            'End If



            LblStatus.Text = "STOP"
            Button1.Text = "START"
        Else
            Dim dir As New IO.DirectoryInfo(LblPath.Text)
            If dir.Exists Then

            Else
                MsgBox("Folder tidak di temukan")
                Exit Sub
            End If
            LblStatus.Text = "RECORDING"
            Button1.Text = "STOP"


        End If

    End Sub


    Private Sub ImportCSV_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MsgBox("Menutup aplikasi akan menghentikan perekaman ke dalam database", MsgBoxStyle.Exclamation + MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then
            e.Cancel = True
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
    

        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            LblPath.Text = FolderBrowserDialog1.SelectedPath
            'My.Settings.Reload = FolderBrowserDialog1.SelectedPath



        End If
    End Sub


    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        NotifyIcon1.Visible = True
        Me.Hide()
        NotifyIcon1.BalloonTipText = "Import data Patlite - TimeLine"
        NotifyIcon1.ShowBalloonTip(500)

    End Sub
    Private Sub Simpan_TimeLine(ByVal Tgl As Date)

        Dim StrSQL As String
        Dim StrMesin, StrMesinBanding As String
        Dim StrWaktuAwal As Date
        Dim StrWaktuAkhir As Date
        Dim intQty, IntRed, intYellow, IntGreen, IntBlue As Integer
        Dim StrJenis, StrJenisDESC, StrJenisBanding As String
        StrJenis = ""
        StrMesinBanding = ""
        Dim intAwalRunning As Integer
       
        'MsgBox(Tgl.ToString("yyyy-MM-dd")) and username ='DC-14' 
        'Exit Sub and  mesin ='DC-14'

        StrSQL = " Select datatime, username,SignalRed, SignalYellow, SignalGreen, SignalBlue,TanggalShift, Tanggal, Dateadd(s,-1,Datatime) waktuawal   " & vbCrLf & _
         "from  DataALLSignalALLMesin_Pertanggal  " & vbCrLf & _
         " Where TanggalShift   ='" & Tgl.ToString("yyyy-MM-dd") & "'     Order by UserName asc,DataTime ASC "

        StrMesin = ""

        Call koneksi2()
        cmd2 = New SqlCommand(" Delete From SignalPatlite_Timeline Where TanggalShift ='" & Tgl.ToString("yyyy-MM-dd") & "'  ", Conn1)
        cmd2.ExecuteNonQuery()
        cmd2.Dispose()
        Conn1.Close()
        Call koneksi2()


        Call koneksi()
        StrJenisBanding = ""
        Using cmd As New SqlCommand(StrSQL, Conn)
            rd = cmd.ExecuteReader
            While rd.Read
                'If RTrim(rd.GetString(1).ToString) = "DC-03" Then
                '    MsgBox("KOSONG")

                'End If
                If StrMesin <> RTrim(rd.GetString(1).ToString) Then
                    'Set parameter awal
                    StrMesin = RTrim(rd.GetString(1).ToString)
                    StrWaktuAwal = rd.GetDateTime(0)
                    StrWaktuAkhir = rd.GetDateTime(0)
                    IntRed = rd.GetValue(2)
                    intYellow = rd.GetValue(3)
                    IntGreen = rd.GetValue(4)
                    IntBlue = rd.GetValue(5)
                    intQty = 0
                    intAwalRunning = 0
                    StrJenis = ""
                    StrJenisBanding = ""
                    Dim dateAwalJam As Date = Tgl.ToString("yyyy-MM-dd") & " 07:00:00.000"
                    Dim HitLong As Long = DateDiff(DateInterval.Second, dateAwalJam, rd.GetDateTime(0))
                    If HitLong < 60 Then
                        'Penyambung Waktu Awal STOP
                        StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                               "Values('" & StrMesin & "','" & dateAwalJam & "','" & rd.GetDateTime(0) & "','" & 0 & "','" & 0 & "','" & 0 & "','" & 1 & "','" & rd.GetDateTime(6) & "','RUN','RUNNING')"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()
                        StrWaktuAkhir = rd.GetDateTime(0)
                        StrWaktuAwal = dateAwalJam
                    Else
                        'Penyambung Waktu Awal RUN
                        StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                               "Values('" & StrMesin & "','" & dateAwalJam & "','" & rd.GetDateTime(0) & "','" & 1 & "','" & 0 & "','" & 0 & "','" & 0 & "','" & rd.GetDateTime(6) & "','STOP','STOP')"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()
                        StrWaktuAkhir = rd.GetDateTime(0)
                        StrWaktuAwal = dateAwalJam


                    End If

                    ' datediff( DateInterval.Second ,rd.GetDateTime(0),

                End If
                If rd.GetValue(5) = 1 Then
                    StrJenis = "RUN"
                    StrJenisDESC = "RUNNING"
                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                        StrJenis = "MOULD"
                        StrJenisDESC = "MOULD CHANGE"
                    End If

                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 1 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then


                        StrJenis = "PLANSTOP"
                        StrJenisDESC = "PLAN STOP"
                    End If
                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                        StrJenis = "STOP"
                        StrJenisDESC = "STOP"
                    End If
                Else
                    If StrJenis = "" Then
                        If StrWaktuAkhir <> rd.GetDateTime(8) Then
                            StrJenis = "STOP"
                            StrJenisDESC = "STOP"
                        End If
                    End If

                End If
                If StrJenis <> StrJenisBanding Then


                    If StrMesinBanding <> "" Then
                        ' StrJenisBanding = StrJenis
                        ' StrWaktuAkhir = rd.GetDateTime(0)
                        'StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(8) & "' where mesin ='" & StrMesinBanding & "' and TanggalAkhir='" & StrWaktuAkhir & "'  "
                        'Ubah atas ok
                        StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(0) & "' where mesin ='" & StrMesinBanding & "' and TanggalAkhir='" & StrWaktuAkhir & "' and jenis ='" & StrJenisBanding & "' and  TanggalAwal ='" & StrWaktuAwal & "'"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()
                    End If

                    StrMesinBanding = StrMesin
                    StrJenisBanding = StrJenis
                    StrWaktuAkhir = rd.GetDateTime(0)
                    If intAwalRunning = 0 Then 'Fungsinya Biar tidak putus jika ada signal stop di awal shift

                        If StrJenis = "STOP" Then
                            ' StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(8) & "' where mesin ='" & StrMesinBanding & "' and TanggalAkhir='" & StrWaktuAkhir & "'  "
                        Else

                            StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                              "Values('" & StrMesin & "','" & rd.GetDateTime(0) & "','" & rd.GetDateTime(0) & "','" & rd.GetValue(2) & "','" & rd.GetValue(3) & "','" & rd.GetValue(4) & "','" & rd.GetValue(5) & "','" & rd.GetDateTime(6) & "','" & StrJenis & "','" & StrJenisDESC & "')"
                            cmd2 = New SqlCommand(StrSQL, Conn1)
                            cmd2.ExecuteNonQuery()
                            cmd2.Dispose()
                            StrWaktuAwal = rd.GetDateTime(0)
                        End If
                    Else
                        StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                              "Values('" & StrMesin & "','" & rd.GetDateTime(0) & "','" & rd.GetDateTime(0) & "','" & rd.GetValue(2) & "','" & rd.GetValue(3) & "','" & rd.GetValue(4) & "','" & rd.GetValue(5) & "','" & rd.GetDateTime(6) & "','" & StrJenis & "','" & StrJenisDESC & "')"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()
                        StrWaktuAwal = rd.GetDateTime(0)
                    End If



                Else
                    StrJenisBanding = StrJenis

                    StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(0) & "' where mesin ='" & StrMesin & "' and TanggalAkhir='" & StrWaktuAkhir & "'  and jenis ='" & StrJenis & "' and  TanggalAwal ='" & StrWaktuAwal & "'"
                    cmd2 = New SqlCommand(StrSQL, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    'StrWaktuAkhir = rd.GetDateTime(8)
                    StrWaktuAkhir = rd.GetDateTime(0)
                End If

                intAwalRunning = intAwalRunning + 1

            End While
            Conn1.Close()

        End Using


        'Menutup Waktu Akhir
        Call SetWaktuAkhir("DC-01", Tgl)
        Call SetWaktuAkhir("DC-02", Tgl)
        Call SetWaktuAkhir("DC-03", Tgl)
        Call SetWaktuAkhir("DC-04", Tgl)
        Call SetWaktuAkhir("DC-05", Tgl)
        Call SetWaktuAkhir("DC-06", Tgl)
        Call SetWaktuAkhir("DC-07", Tgl)
        Call SetWaktuAkhir("DC-08", Tgl)
        Call SetWaktuAkhir("DC-09", Tgl)
        Call SetWaktuAkhir("DC-10", Tgl)
        Call SetWaktuAkhir("DC-11", Tgl)
        Call SetWaktuAkhir("DC-12", Tgl)
        Call SetWaktuAkhir("DC-13", Tgl)
        Call SetWaktuAkhir("DC-14", Tgl)
        Call SetWaktuAkhir("DC-15", Tgl)
        Call SetWaktuAkhir("DC-16", Tgl)
        Call SetWaktuAkhir("DC-17", Tgl)
        Call SetWaktuAkhir("DC-18", Tgl)
        Call SetWaktuAkhir("DC-19", Tgl)
        Call SetWaktuAkhir("DC-20", Tgl)

    End Sub
    Private Sub SetWaktuAkhir(ByVal Mesin As String, ByVal Tgl As Date)
        Dim StrSQl As String


        StrSQl = " Select  TOP 1 Mesin, TanggalAwal, TanggalAkhir, PartNumber, PartName, Cavity, Qty, tanggalShift, Jenis, JenisDesc" & vbCrLf & _
        "from  SignalPatlite_Timeline  " & vbCrLf & _
        " Where TanggalShift   ='" & Tgl.ToString("yyyy-MM-dd") & "'   AND MESIN='" & Mesin & "' Order by  TanggalAkhir DESC"

        Call koneksi()

        Using cmd As New SqlCommand(StrSQl, Conn)
            rd = cmd.ExecuteReader
            While rd.Read
                If rd.GetValue(8) = "STOP" Then
                    Call koneksi2()
                    StrSQl = "Update SignalPatlite_Timeline Set TanggalAkhir='" & DateAdd(DateInterval.Day, 1, Tgl).ToString("yyyy-MM-dd") & " 07:00:00.000'   Where TanggalShift ='" & Tgl.ToString("yyyy-MM-dd") & "'  and mesin ='" & Mesin & "' and TanggalAkhir ='" & rd.GetValue(2) & "'"
                    cmd2 = New SqlCommand(StrSQl, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    Conn1.Close()
                ElseIf rd.GetValue(8) = "MOULD" Then
                    Call koneksi2()
                    StrSQl = "Update SignalPatlite_Timeline Set TanggalAkhir='" & DateAdd(DateInterval.Day, 1, Tgl).ToString("yyyy-MM-dd") & " 07:00:00.000'   Where TanggalShift ='" & Tgl.ToString("yyyy-MM-dd") & "'  and mesin ='" & Mesin & "' and TanggalAkhir ='" & rd.GetValue(2) & "'"
                    cmd2 = New SqlCommand(StrSQl, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    Conn1.Close()
                ElseIf rd.GetValue(8) = "PLANSTOP" Then
                    Call koneksi2()
                    StrSQl = "Update SignalPatlite_Timeline Set TanggalAkhir='" & DateAdd(DateInterval.Day, 1, Tgl).ToString("yyyy-MM-dd") & " 07:00:00.000'   Where TanggalShift ='" & Tgl.ToString("yyyy-MM-dd") & "'  and mesin ='" & Mesin & "' and TanggalAkhir ='" & rd.GetValue(2) & "'"
                    cmd2 = New SqlCommand(StrSQl, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    Conn1.Close()
                ElseIf rd.GetValue(8) = "RUN" Then
                    Call koneksi2()
                    StrSQl = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                               "Values('" & rd.GetString(0) & "','" & rd.GetDateTime(2) & "','" & DateAdd(DateInterval.Day, 1, Tgl).ToString("yyyy-MM-dd") & " 07:00:00.000','1','0','0','0','" & Tgl.ToString("yyyy-MM-dd") & "','STOP','STOP')"

                    cmd2 = New SqlCommand(StrSQl, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    Conn1.Close()
                End If
                Conn.Close()
                Exit Sub
            End While
        End Using
        Conn.Close()

    End Sub
    Private Sub Simpan_TimeLineV1()

        Dim StrSQL As String
        Dim StrMesin, StrMesinBanding As String
        Dim StrWaktuAwal As Date
        Dim StrWaktuAkhir As Date
        Dim intQty, IntRed, intYellow, IntGreen, IntBlue As Integer
        Dim StrJenis, StrJenisDESC, StrJenisBanding As String
        StrJenis = ""
        StrMesinBanding = ""
        'Per Part
        'StrSQL = "select Distinct  tanggal ,Mesin ,Coalesce(PartNumber,'') as PartNumber,Coalesce(PartName,'') as PartName, coalesce(Cavity,'') as Cavity  ,COALESCE(Qty,0) AS Qty, TanggalShift from DataSignalBluePatLite " & vbCrLf & _
        '         "Where TanggalShift ='2018-07-17' " & vbCrLf & _
        '           "Order by Mesin , Tanggal  ASC"

        'Per Mesin
        StrSQL = " Select datatime, username,SignalRed, SignalYellow, SignalGreen, SignalBlue,TanggalShift, Tanggal, Dateadd(s,-1,Datatime) waktuawal   " & vbCrLf & _
         "from  DataALLSignalALLMesin_Pertanggal  " & vbCrLf & _
         " Where TanggalShift   ='2018-07-26'    Order by UserName,DataTime ASC "

        StrMesin = ""

        Call koneksi()
        Call koneksi2()


        cmd2 = New SqlCommand(" Delete From SignalPatlite_Timeline Where TanggalShift ='2018-07-26' ", Conn1)
        cmd2.ExecuteNonQuery()
        cmd2.Dispose()
        Conn1.Close()
        Call koneksi2()


        StrJenisBanding = ""
        Using cmd As New SqlCommand(StrSQL, Conn)
            rd = cmd.ExecuteReader
            While rd.Read
                If StrMesin <> RTrim(rd.GetString(1).ToString) Then
                    'Set parameter awal
                    StrMesin = RTrim(rd.GetString(1).ToString)
                    StrWaktuAwal = rd.GetDateTime(0)
                    StrWaktuAkhir = rd.GetDateTime(0)
                    IntRed = rd.GetValue(2)
                    intYellow = rd.GetValue(3)
                    IntGreen = rd.GetValue(4)
                    IntBlue = rd.GetValue(5)
                    intQty = 0
                    'If rd.GetValue(5) = 1 Then
                    '    StrJenis = "RUN"
                    '    StrJenisDESC = "RUNNING"

                    'ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    '    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                    '        StrJenis = "MOULD"
                    '        StrJenisDESC = "MOULD CHANGE"
                    '    End If

                    'ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 1 And rd.GetValue(5) = 0 Then
                    '    If StrWaktuAkhir <> rd.GetDateTime(8) Then

                    '        StrJenis = "PLANSTOP"
                    '        StrJenisDESC = "PLAN STOP"
                    '    End If
                    'ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    '    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                    '        StrJenis = "STOP"
                    '        StrJenisDESC = "STOP"
                    '    End IF
                    'End If
                End If
                If rd.GetValue(5) = 1 Then
                    StrJenis = "RUN"
                    StrJenisDESC = "RUNNING"
                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                        StrJenis = "MOULD"
                        StrJenisDESC = "MOULD CHANGE"
                    End If

                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 1 And rd.GetValue(4) = 1 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then


                        StrJenis = "PLANSTOP"
                        StrJenisDESC = "PLAN STOP"
                    End If
                ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                    If StrWaktuAkhir <> rd.GetDateTime(8) Then
                        StrJenis = "STOP"
                        StrJenisDESC = "STOP"
                    End If
                End If
                If StrJenis <> StrJenisBanding Then


                    If StrMesinBanding <> "" Then
                        ' StrJenisBanding = StrJenis
                        ' StrWaktuAkhir = rd.GetDateTime(0)
                        StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(8) & "' where mesin ='" & StrMesinBanding & "' and TanggalAkhir='" & StrWaktuAkhir & "' and TanggalAwal ='" & StrWaktuAwal & "'"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()
                    End If

                    StrMesinBanding = StrMesin
                    StrJenisBanding = StrJenis
                    StrWaktuAkhir = rd.GetDateTime(0)
                    StrWaktuAwal = rd.GetDateTime(0)
                    StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                               "Values('" & StrMesin & "','" & rd.GetDateTime(0) & "','" & rd.GetDateTime(0) & "','" & rd.GetValue(2) & "','" & rd.GetValue(3) & "','" & rd.GetValue(4) & "','" & rd.GetValue(5) & "','" & rd.GetDateTime(6) & "','" & StrJenis & "','" & StrJenisDESC & "')"
                    cmd2 = New SqlCommand(StrSQL, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()

                Else
                    StrJenisBanding = StrJenis

                    StrSQL = "Update  SignalPatlite_Timeline set Tanggalakhir ='" & rd.GetDateTime(8) & "' where mesin ='" & StrMesin & "' and TanggalAkhir='" & StrWaktuAkhir & "' and  TanggalAwal ='" & StrWaktuAwal & "'"
                    cmd2 = New SqlCommand(StrSQL, Conn1)
                    cmd2.ExecuteNonQuery()
                    cmd2.Dispose()
                    StrWaktuAkhir = rd.GetDateTime(8)
                End If


            End While
            Conn1.Close()
        End Using
    End Sub
    Private Sub Simpan_TimeLineOri()

        Dim StrSQL As String
        Dim StrMesin As String
        Dim StrWaktuAwal As Date
        Dim StrWaktuAkhir As Date
        Dim intQty As Integer
        Dim StrJenis As String
        StrJenis = ""
        'Per Part
        'StrSQL = "select Distinct  tanggal ,Mesin ,Coalesce(PartNumber,'') as PartNumber,Coalesce(PartName,'') as PartName, coalesce(Cavity,'') as Cavity  ,COALESCE(Qty,0) AS Qty, TanggalShift from DataSignalBluePatLite " & vbCrLf & _
        '         "Where TanggalShift ='2018-07-17' " & vbCrLf & _
        '           "Order by Mesin , Tanggal  ASC"

        'Per Mesin
        StrSQL = " Select datatime, username,SignalRed, SignalYellow, SignalGreen, SignalBlue,TanggalShift, Tanggal  " & vbCrLf & _
         "from  DataALLSignalALLMesin_Pertanggal  " & vbCrLf & _
         " Where TanggalShift   ='2018-07-23' Order by UserName,DataTime ASC "

        StrMesin = ""

        Call koneksi()
        Call koneksi2()


        cmd2 = New SqlCommand(" Delete From SignalPatlite_Timeline Where TanggalShift ='2018-07-23' ", Conn1)
        cmd2.ExecuteNonQuery()
        cmd2.Dispose()
        Conn1.Close()
        Call koneksi2()
        '2018-04-30'

        Using cmd As New SqlCommand(StrSQL, Conn)
            rd = cmd.ExecuteReader
            While rd.Read
                If StrMesin <> RTrim(rd.GetString(1).ToString) Then
                    'Set parameter awal
                    StrMesin = RTrim(rd.GetString(1).ToString)
                    StrWaktuAwal = rd.GetDateTime(0)
                    StrWaktuAkhir = rd.GetDateTime(0)

                    'MsgBox(StrMesin & " " & StrWaktuAwal & " " & StrWaktuAkhir)
                    intQty = 0
                Else
                    ' intQty = intQty + 1 'rd.GetValue(5)
                    'Set parameter kedua atau stop 

                    'MsgBox(StrMesin & " - Tgl AWal :  " & StrWaktuAkhir & " - Tgl Akhir : " & rd.GetDateTime(0) & " - Detik : " & DateDiff(DateInterval.Second, StrWaktuAkhir, rd.GetDateTime(0)))
                    ' If DateDiff(DateInterval.Second, StrWaktuAkhir, rd.GetDateTime(0)) > 100 Then
                    'MsgBox(StrMesin & " - Tgl AWal :  " & StrWaktuAwal & " - Tgl Akhir : " & StrWaktuAkhir & " - Detik : " & DateDiff(DateInterval.Second, StrWaktuAkhir, rd.GetDateTime(0)))

                    '#RUN TIME
                    If rd.GetValue(2) = 0 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 1 Then

                        'If StrJenis <> "RUN" Then
                        '    cmd2 = New SqlCommand(StrSQL, Conn1)
                        '    cmd2.ExecuteNonQuery()
                        '    cmd2.Dispose()

                        'End If
                        StrJenis = "RUN"

                        'StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                        '   "Values('" & StrMesin & "','" & StrWaktuAwal & "','" & StrWaktuAkhir & "','" & rd.GetValue(2) & "','" & rd.GetValue(3) & "','" & rd.GetValue(4) & "','" & rd.GetValue(5) & "','" & rd.GetDateTime(6) & "','RUN','RUNNING')"


                        'StrWaktuAwal = rd.GetDateTime(0)
                        'StrWaktuAkhir = rd.GetDateTime(0)

                        'intQty = 0

                    ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 0 Then
                        '#STOP TIME

                        If StrJenis <> "STOP" Then
                            cmd2 = New SqlCommand(StrSQL, Conn1)
                            cmd2.ExecuteNonQuery()
                            cmd2.Dispose()
                            StrWaktuAwal = rd.GetDateTime(0)
                        End If
                        StrJenis = "STOP"

                        StrSQL = "insert into SignalPatlite_Timeline (Mesin , TanggalAwal,Tanggalakhir ,PartNumber,PartName, Cavity ,Qty, tanggalShift, jenis, jenisdesc) " & vbCrLf & _
                           "Values('" & StrMesin & "','" & StrWaktuAwal & "','" & StrWaktuAkhir & "','" & rd.GetValue(2) & "','" & rd.GetValue(3) & "','" & rd.GetValue(4) & "','" & rd.GetValue(5) & "','" & rd.GetDateTime(6) & "','STOP','STOP')"
                        cmd2 = New SqlCommand(StrSQL, Conn1)
                        cmd2.ExecuteNonQuery()
                        cmd2.Dispose()


                        StrWaktuAwal = rd.GetDateTime(0)
                        intQty = 0

                    ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 1 Then
                        '#MOULD CHANGE
                        StrJenis = "RUN"
                    ElseIf rd.GetValue(2) = 1 And rd.GetValue(3) = 0 And rd.GetValue(4) = 0 And rd.GetValue(5) = 1 Then
                        '#STOP PLAN
                        StrJenis = "RUN"

                    End If

                    'End If
                    StrWaktuAkhir = rd.GetDateTime(0)

                End If


            End While
            Conn1.Close()
        End Using
    End Sub
    Private Sub Import_CSV()
        MsgBox("Fault : Simpan1")
        Exit Sub

        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")
        File = LblPath.Text & "\patlog_auto" & dateAsString & ".csv"




        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & _
                    "is not valid and will be skipped.")
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()
      

        '
        

    End Sub
    Private Sub Import_CSVPerJam7()
        MsgBox("Fault : Simpan1")
        Exit Sub

        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now() '.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")
        File = LblPath.Text & "\patlog_auto" & dateAsString & ".csv"




        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & _
                    "is not valid and will be skipped.")
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()


        ''


    End Sub
    Private Sub Import_CSVAdditional()
        MsgBox("Fault : Simpan1")
        Exit Sub

        ' File = "E:\Data Resta\patlite\patlog_auto2018_02_21.csv"
        Dim yesterday As DateTime = Now.AddDays(-1)
        Dim dateAsString = yesterday.ToString("yyyy_MM_dd")

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            Dim fInfo = New System.IO.FileInfo(OpenFileDialog1.FileName)

            File = fInfo.DirectoryName & "\" & fInfo.Name

        Else
            Exit Sub
        End If


        Me.Cursor = Cursors.WaitCursor


        ' File = LblPath.Text '& "\patlog_auto" & dateAsString & ".csv"


        Dim TextFileReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(File)

        TextFileReader.TextFieldType = FileIO.FieldType.Delimited
        TextFileReader.SetDelimiters(",")

        Dim TextFileTable As DataTable = Nothing

        Dim Column As DataColumn
        Dim Row As DataRow
        Dim UpperBound As Int32
        Dim ColumnCount As Int32
        Dim CurrentRow As String()

        Dim Baris As Single
        Baris = 0
        While Not TextFileReader.EndOfData
            Baris = Baris + 1
            If Baris <> 1 Then


                Try

                    CurrentRow = TextFileReader.ReadFields()

                    If Not CurrentRow Is Nothing Then
                        ''# Check if DataTable has been created
                        If TextFileTable Is Nothing Then
                            TextFileTable = New DataTable("TextFileTable")
                            ''# Get number of columns
                            UpperBound = CurrentRow.GetUpperBound(0)
                            ''# Create new DataTable
                            For ColumnCount = 0 To UpperBound
                                Column = New DataColumn()
                                Column.DataType = System.Type.GetType("System.String")
                                Column.ColumnName = "Column" & ColumnCount
                                Column.Caption = "Column" & ColumnCount
                                Column.ReadOnly = True
                                Column.Unique = False
                                TextFileTable.Columns.Add(Column)
                            Next

                            'Custom Column
                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "ColumnPath"
                            Column.Caption = "Path"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)

                            Column = New DataColumn()
                            Column.DataType = System.Type.GetType("System.String")
                            Column.ColumnName = "Columndate"
                            Column.Caption = "Date"
                            Column.ReadOnly = True
                            Column.Unique = False
                            TextFileTable.Columns.Add(Column)


                        End If
                        Row = TextFileTable.NewRow
                        ' MsgBox("" & UpperBound)
                        For ColumnCount = 0 To UpperBound
                            Row("Column" & ColumnCount) = CurrentRow(ColumnCount).ToString
                            Row("ColumnPath") = File
                            Row("Columndate") = Now()
                        Next
                        TextFileTable.Rows.Add(Row)
                    End If
                Catch ex As  _
                Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & _
                    "is not valid and will be skipped.")
                End Try
            End If
        End While
        TextFileReader.Dispose()
        DataGridView1.DataSource = TextFileTable

        If DataGridView1.Rows(0).Cells(0).Value = "DataTime" Then
            DataGridView1.Rows.Remove(DataGridView1.Rows(0))
        End If

        koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()


        Using bcp As New SqlClient.SqlBulkCopy(str)
            bcp.DestinationTableName = "Patlite_import"
            bcp.BatchSize = 1000
            bcp.WriteToServer(TextFileTable)
        End Using

        '  Simpan1()


        ''


    End Sub
    Private Sub Simpan1()
        MsgBox("Fault : Simpan1")
        Exit Sub

        Call koneksi()
        Dim StrSQL As String = ""
        StrSQL = "Delete From Patlite_import  Where FileImport like '" & File & "' "
        cmd = New SqlCommand(StrSQL, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
        Conn.Close()

        Dim connection As New Data.SqlClient.SqlConnection
        Dim command As New Data.SqlClient.SqlCommand

        connection.ConnectionString = str '"Server= server; Database= DB; integrated security=true"
        command.CommandText = "INSERT INTO Patlite_import (DataTime, MacAddress, UserName, SignalRed,SignalYellow,SignalGreen,SignalBlue,SignalWhite,FileImport,TanggalImport) VALUES (@DataTime, @MacAddress, @UserName, @SignalRed,@SignalYellow,@SignalGreen,@SignalBlue, @SignalWhite, @FileImport, @TanggalImport)"

        command.Parameters.Add("@DataTime", SqlDbType.DateTime)
        command.Parameters.Add("@MacAddress", SqlDbType.NVarChar)
        command.Parameters.Add("@UserName", SqlDbType.NVarChar)
        command.Parameters.Add("@SignalRed", SqlDbType.Int)
        command.Parameters.Add("@SignalYellow", SqlDbType.Int)
        command.Parameters.Add("@SignalGreen", SqlDbType.Int)
        command.Parameters.Add("@SignalBlue", SqlDbType.Int)
        command.Parameters.Add("@SignalWhite", SqlDbType.Int)
        command.Parameters.Add("@FileImport", SqlDbType.Text)
        command.Parameters.Add("@TanggalImport", SqlDbType.DateTime)


        connection.Open()
        command.Connection = connection

        For i As Integer = 0 To DataGridView1.Rows.Count - 1

            command.Parameters(0).Value = DataGridView1.Rows(i).Cells(0).Value
            command.Parameters(1).Value = DataGridView1.Rows(i).Cells(1).Value
            command.Parameters(2).Value = DataGridView1.Rows(i).Cells(2).Value
            command.Parameters(3).Value = DataGridView1.Rows(i).Cells(3).Value
            command.Parameters(4).Value = DataGridView1.Rows(i).Cells(4).Value
            command.Parameters(5).Value = DataGridView1.Rows(i).Cells(5).Value
            command.Parameters(6).Value = DataGridView1.Rows(i).Cells(6).Value
            command.Parameters(7).Value = DataGridView1.Rows(i).Cells(7).Value
            command.Parameters(8).Value = DataGridView1.Rows(i).Cells(8).Value
            command.Parameters(9).Value = Now() 'DataGridView1.Rows(i).Cells(9).Value

            If i <> DataGridView1.Rows.Count - 1 Then
                command.ExecuteNonQuery()
            End If
        Next
    End Sub
    Private Sub ImportCSV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim reader As New System.Configuration.AppSettingsReader
        LblPath.Text = reader.GetValue("URL", GetType(String))
        DT.Value = DateTime.Now


    End Sub

 

    Private Sub LblTimer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblTimer.TextChanged
        StrWaktu = Format(Now, "hh:mm:ss")
        Dim reader As New System.Configuration.AppSettingsReader

        If RTrim(StrWaktu.Substring(StrWaktu.Length - 8, 8)) = RTrim(reader.GetValue("TIMESET", GetType(String))) Then

            If Button1.Text = "STOP" Then

                'Import_CSV()
                Me.Cursor = Cursors.WaitCursor
                ' Simpan_TimeLine(DT.Value)
                Me.Cursor = Cursors.Default
                lblsaved.Text = StrWaktu
            End If
        End If
        'If RTrim(StrWaktu.Substring(StrWaktu.Length - 8, 8)) = RTrim(reader.GetValue("TIMEDOWN", GetType(String))) Then

        '    If Button1.Text = "STOP" Then

        '        'Import_CSVPerJam7()
        '        Me.Cursor = Cursors.WaitCursor
        '        Simpan_TimeLine()
        '        Me.Cursor = Cursors.Default
        '        lblsaved.Text = StrWaktu
        '    End If
        'End If

    End Sub


    Private Sub BtnAdditionalImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdditionalImport.Click
        Me.Cursor = Cursors.WaitCursor
        Call Simpan_TimeLine(DT.Value)

        Me.Cursor = Cursors.Default
        lblsaved.Text = StrWaktu
    End Sub
End Class
